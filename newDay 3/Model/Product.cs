﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newDay_3.Model
{
    internal class Product
    {
        internal int data;

        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public int Price { get; set; }
        public float Rating { get; set; }

        public Product(int id, string name, string category, int price, float rating)
        {
            Id = id;
            Name = name;
            Category = category;
            Price = price;
            Rating = rating;
        }

        public override string ToString()
        {
            return $"ID:{Id} \t Name:{Name} \t Categoy:{Category} \t Price:{Price} \t Rating:{Rating}";
        }
    }
}
