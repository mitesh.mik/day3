﻿using newDay_3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newDay_3.Repository
{
    internal class ProductRepository
    {
        //declare class array
        Product[] products;
        public ProductRepository()
        {
            products = new Product[2];
            //{
            // new Product(1,"oppo","mobile",15000,4.5f),
            //new Product ( 2, "vivo", "mobile", 20000, 4.5f),
            //new Product (3, "lenovo", "laptop", 50000, 4.5f ),
            //new Product ( 4, "samsung", "TV", 25000, 4.5f )
        //};
        }

        public Product[] getprodctDetail()
        {
            return products;
        }
        public void AddProduct()
        {
            for(int i = 0; i < 2; i++)
            {
                Console.WriteLine("enter Id");
                var ID = int.Parse(Console.ReadLine());
                Console.WriteLine("enter category");
                var Category = Console.ReadLine();
                Console.WriteLine("enter  name");
                var Name = Console.ReadLine();
                Console.WriteLine("enter price");
                var Price = int.Parse(Console.ReadLine());
                Console.WriteLine("enter rating");
                var Rating = float.Parse(Console.ReadLine());
                products[i] = new Product(ID, Name,Category,Price, Rating);
            }

        }
        public  void Getcategory()
        {
            Console.WriteLine("enter category want to display");
            var category = Console.ReadLine();
            var pro=Array.FindAll(products, item => item.Category == category);
            foreach(var item in pro)
            {
                Console.WriteLine(item);
            }
            
        }
        public void Deletecategory()
        {
            Console.WriteLine("enter Id want to delete");
            var ID = int.Parse(Console.ReadLine());
            var del = Array.Find(products, item => item.Id == ID);

            if (del==null )
            {
                throw new ProductException("ID doesnot exists");
            }
            else
            {
                var pro = Array.FindAll(products, item => item.Id != ID);
                foreach (var item in pro)
                {
                    Console.WriteLine(item);
                }
            }
        }

        public void Update()
        {
            Console.WriteLine("enter id want to update");
            var ID=int.Parse(Console.ReadLine());
            Console.WriteLine("enter category");
            var Category = Console.ReadLine();
            Console.WriteLine("enter  name");
            var Name = Console.ReadLine();
            Console.WriteLine("enter price");
            var Price = int.Parse(Console.ReadLine());
            Console.WriteLine("enter rating");
            var Rating = float.Parse(Console.ReadLine());
     


            var up = Array.FindIndex(products, item => item.Id == ID);
            //foreach (var item in up)
            //{
            //     item.data == val;
            //}
            products[up].Category = Category;
            products[up].Price = Price;
            products[up].Name = Name;
            products[up].Rating = (float)Rating;


            foreach (var item in products)
            {
                Console.WriteLine(item);
            }
        }
        
    }
}
